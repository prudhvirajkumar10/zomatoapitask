package com.task.hopscotch.repositories

import com.task.hopscotch.models.CityData
import com.task.hopscotch.models.CollectionData
import com.task.hopscotch.models.OutletData
import com.task.hopscotch.network.TaskNetworkHelper
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TaskRepository @Inject constructor(
    private val networkHelper: TaskNetworkHelper
) {

    suspend fun getCity(latitude: Double, longitude: Double): Response<CityData> {
        return networkHelper.getCity(latitude, longitude)
    }

    suspend fun getCollections(cityId: Int, count: Int): Response<CollectionData> {
        return networkHelper.getCollections(cityId, count)
    }

    suspend fun getLocationDetails(cityId: Int, type: String = "city"): Response<OutletData> {
        return networkHelper.getLocationDetails(cityId, type)
    }
}