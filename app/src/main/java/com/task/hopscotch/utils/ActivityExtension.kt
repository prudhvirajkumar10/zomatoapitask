package com.task.hopscotch.utils

import android.app.Activity
import android.view.View
import com.task.hopscotch.views.MainActivity

fun Activity.showProgress() {
    (this as MainActivity).binding.spinKit.visibility = View.VISIBLE
}

fun Activity.hideProgress() {
    (this as MainActivity).binding.spinKit.visibility = View.GONE
}