package com.task.hopscotch.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.task.hopscotch.R
import com.task.hopscotch.adapters.RestaurantAdapter
import com.task.hopscotch.databinding.ExploreFragmentBinding
import com.task.hopscotch.models.ResponseStatus
import com.task.hopscotch.utils.hideProgress
import com.task.hopscotch.utils.showProgress
import com.task.hopscotch.viewmodels.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ExploreFragment : Fragment() {

    private val viewModel: HomeViewModel by navGraphViewModels(R.id.navigation_graph) {
        defaultViewModelProviderFactory
    }

    @Inject
    lateinit var restaurantAdapter: RestaurantAdapter

    private lateinit var binding: ExploreFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ExploreFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.fetchCityData(12.971606, 77.594376)
        setupObserver()
        bindInterface()
    }

    private fun bindInterface() {
        binding.apply {
            restaurant.layoutManager = LinearLayoutManager(requireContext())
            restaurant.adapter = restaurantAdapter
        }
    }

    private fun setupObserver() {
        viewModel.cityData.observe(viewLifecycleOwner) { task ->
            when(task.responseStatus) {
                ResponseStatus.SUCCESS -> {
                    activity?.hideProgress()
                    val data = task.data
                    data?.nearbyRestaurants?.let {
                        val restaurants = it.map { element -> element.restaurant }
                        restaurantAdapter.restaurants.clear()
                        restaurantAdapter.restaurants.addAll(restaurants)
                        restaurantAdapter.notifyDataSetChanged()
                    }
                }
                ResponseStatus.LOADING -> {
                    activity?.showProgress()
                }
                ResponseStatus.ERROR -> {
                    activity?.hideProgress()
                    Toast.makeText(requireContext(), task.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }


}