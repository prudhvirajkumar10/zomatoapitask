package com.task.hopscotch.views

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.*
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.kingfisher.easyviewindicator.GridLayoutSnapHelper
import com.task.hopscotch.R
import com.task.hopscotch.adapters.AutoCollectionAdapter
import com.task.hopscotch.adapters.CuisineAdapter
import com.task.hopscotch.adapters.ManualOutletAdapter
import com.task.hopscotch.adapters.RestaurantAdapter
import com.task.hopscotch.databinding.HomeFragmentBinding
import com.task.hopscotch.models.CollectionCollection
import com.task.hopscotch.models.ManualOutlet
import com.task.hopscotch.models.ResponseStatus
import com.task.hopscotch.models.Restaurant
import com.task.hopscotch.utils.hideProgress
import com.task.hopscotch.utils.showProgress
import com.task.hopscotch.viewmodels.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : Fragment() {

    companion object {
        private const val UPDATE_INTERVAL_IN_SECONDS: Long = 10000
        private const val FASTEST_UPDATE_INTERVAL_IN_SECONDS: Long = 2000
        private const val REQUEST_TURN_DEVICE_LOCATION_ON = 1995
    }

    private lateinit var binding: HomeFragmentBinding

    private val viewModel: HomeViewModel by navGraphViewModels(R.id.navigation_graph) {
        defaultViewModelProviderFactory
    }

    @Inject
    lateinit var autoAdapter: AutoCollectionAdapter
    @Inject
    lateinit var manualOutletAdapter: ManualOutletAdapter
    @Inject
    lateinit var cuisineAdapter: CuisineAdapter
    @Inject
    lateinit var restaurantAdapter: RestaurantAdapter

    private val fusedLocationClient by lazy {
        LocationServices.getFusedLocationProviderClient(
            requireContext()
        )
    }
    private lateinit var locationRequest: LocationRequest
    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            super.onLocationResult(locationResult)
            locationResult?.lastLocation?.let {
                viewModel.fetchCityData(it.latitude, it.longitude)
            }
        }
    }

    private val requestPermission = registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
        if (isGranted) {
            buildLocationRequest()
        } else {
            showLocationRequiredAlert()
        }
    }

    private var autoCollectionData: List<CollectionCollection>? = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = HomeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkPermission()
        setupObserver()
        bindInterface()
    }

    private fun setupObserver() {
        //Fetch the city Id
        viewModel.cityData.observe(viewLifecycleOwner) { task ->
            when(task.responseStatus) {
                ResponseStatus.SUCCESS -> {
                    activity?.hideProgress()
                    val data = task.data
                    data?.location?.let { address ->
                        binding.addressLabel.text = address.cityName
                        viewModel.fetchCollections(address.cityId)
                        viewModel.fetchOutletData(address.cityId)
                    }

                    data?.popularity?.let {popular ->
                        cuisineAdapter.data.clear()
                        cuisineAdapter.data.addAll(popular.topCuisines)
                        cuisineAdapter.data.addAll(popular.topCuisines.subList(0, 1))
                        cuisineAdapter.data.shuffle(Random())
                        cuisineAdapter.notifyDataSetChanged()
                    }

                    data?.nearbyRestaurants?.let {elements ->
                        val restaurants = elements.map { element -> element.restaurant }
                        restaurantAdapter.restaurants.clear()
                        restaurantAdapter.restaurants.addAll(restaurants)
                        restaurantAdapter.notifyDataSetChanged()
                    }
                }
                ResponseStatus.LOADING -> {
                    activity?.showProgress()
                }
                ResponseStatus.ERROR -> {
                    activity?.hideProgress()
                    Toast.makeText(requireContext(), task.message, Toast.LENGTH_LONG).show()
                }
            }
        }

        viewModel.collectionData.observe(viewLifecycleOwner) {
            when(it.responseStatus) {
                ResponseStatus.SUCCESS -> {
                    activity?.hideProgress()
                    val data = it.data
                    autoCollectionData = data?.collections?.map { element ->
                        element.collection
                    }
                    autoCollectionData?.let { elements ->
                        autoAdapter.autoCollectionData.clear()
                        autoAdapter.autoCollectionData.addAll(elements)
                        autoAdapter.notifyDataSetChanged()
                        binding.autoIndicator.forceUpdateItemCount()
                    }
                }
                ResponseStatus.LOADING -> {
                    activity?.showProgress()
                }
                ResponseStatus.ERROR -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
                }
            }
        }

        viewModel.outletData.observe(viewLifecycleOwner) {
            when(it.responseStatus) {
                ResponseStatus.SUCCESS -> {
                    activity?.hideProgress()
                    val data = it.data
                    val manualOutlets = mutableListOf<ManualOutlet>()
                    data?.bestRatedRestaurant?.let { list ->

                        for (i in list.indices step 2) {
                            val restaurants = mutableListOf<Restaurant>()
                            if (i % 2 == 0) {
                                restaurants.add(list[i].restaurant)
                                if (i < list.size - 1) {
                                    restaurants.add(list[i + 1].restaurant)
                                }
                            }
                            val manualOutlet = ManualOutlet(restaurants)
                            manualOutlets.add(manualOutlet)
                        }
                    }
                    manualOutlets.let {elements ->
                        manualOutletAdapter.manualOutletData.clear()
                        manualOutletAdapter.manualOutletData.addAll(elements)
                        manualOutletAdapter.notifyDataSetChanged()
                    }
                }
                ResponseStatus.LOADING -> {
                    activity?.showProgress()
                }
                ResponseStatus.ERROR -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun bindInterface() {
        binding.apply {
            val snapHelper = LinearSnapHelper()
            snapHelper.attachToRecyclerView(autoCarousel)
            autoCarousel.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            autoCarousel.adapter = autoAdapter
            autoIndicator.setRecyclerView(autoCarousel)

            val snapHelper2 = GridLayoutSnapHelper(2)
            snapHelper2.attachToRecyclerView(manualCarousel)
            manualCarousel.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            manualCarousel.adapter = manualOutletAdapter

            cuisineCarousel.layoutManager = GridLayoutManager(requireContext(), 3)
            cuisineCarousel.adapter = cuisineAdapter

            nearbyCarousel.layoutManager = LinearLayoutManager(requireContext())
            nearbyCarousel.adapter = restaurantAdapter
        }
    }

    private fun checkPermission() {
        when {
            ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED -> {
                buildLocationRequest()
            }
            shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) -> {
                showLocationRequiredAlert()
            }
            else -> {
                requestLocationPermission()
            }
        }
    }

    private fun requestLocationPermission() {
        requestPermission.launch(Manifest.permission.ACCESS_FINE_LOCATION)
    }

    private fun showLocationRequiredAlert() {
        val alert = AlertDialog.Builder(requireActivity())
            .setMessage(getString(R.string.location_permission_request_message))
            .setPositiveButton("Enable") { dialog, _ ->
                dialog.dismiss()
                requestLocationPermission()
            }
            .setNegativeButton("Ignore") { dialog, _ ->
                dialog.cancel()
            }
            .create()
        alert.show()
    }

    private fun buildLocationRequest() {
        val client: SettingsClient = LocationServices.getSettingsClient(requireContext())
        locationRequest = LocationRequest.create().apply {
            interval = TimeUnit.SECONDS.toMillis(UPDATE_INTERVAL_IN_SECONDS)
            fastestInterval = TimeUnit.SECONDS.toMillis(FASTEST_UPDATE_INTERVAL_IN_SECONDS)
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        val locationBuilder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)

        activity?.apply {
            client.checkLocationSettings(locationBuilder.build())
                .addOnSuccessListener {
                    fetchLocation()
                }
                .addOnFailureListener { exception ->
                    if (exception is ResolvableApiException) {
                        try {
                            exception.startResolutionForResult(
                                this,
                                REQUEST_TURN_DEVICE_LOCATION_ON
                            )
                        } catch (e: IntentSender.SendIntentException) {

                        }
                    }
                }
        }
    }

    @Suppress("DEPRECATION")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_TURN_DEVICE_LOCATION_ON -> {
                when (resultCode) {
                    Activity.RESULT_OK -> buildLocationRequest()
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun fetchLocation() {
        fusedLocationClient?.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.myLooper()
        )
    }

    override fun onDestroy() {
        fusedLocationClient?.removeLocationUpdates(locationCallback)
        super.onDestroy()
    }
}