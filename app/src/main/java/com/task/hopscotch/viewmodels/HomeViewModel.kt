package com.task.hopscotch.viewmodels

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.task.hopscotch.models.CityData
import com.task.hopscotch.models.CollectionData
import com.task.hopscotch.models.OutletData
import com.task.hopscotch.models.TaskResponse
import com.task.hopscotch.network.ConnectionHelper
import com.task.hopscotch.repositories.TaskRepository
import kotlinx.coroutines.launch

class HomeViewModel @ViewModelInject constructor(
    private val taskRepository: TaskRepository,
    private val connectionHelper: ConnectionHelper
) : ViewModel() {

    // cached
    private val _cityData = MutableLiveData<TaskResponse<CityData>>()
    // public
    val cityData: LiveData<TaskResponse<CityData>>
        get() = _cityData

    fun fetchCityData(latitude: Double, longitude: Double) = viewModelScope.launch {
        _cityData.postValue(TaskResponse.loading(null))
        if (connectionHelper.isNetworkConnected()) {
            taskRepository.getCity(latitude, longitude).let {
                if (it.isSuccessful) {
                    _cityData.postValue(TaskResponse.success(it.body()))
                } else {
                    _cityData.postValue(TaskResponse.error(null, "Failed to place network request"))
                }
            }
        } else {
            _cityData.postValue(TaskResponse.error(data = null, message = "No Internet Connection"))
        }
    }


    // cached
    private val _collectionData = MutableLiveData<TaskResponse<CollectionData>>()
    // public
    val collectionData: LiveData<TaskResponse<CollectionData>>
        get() = _collectionData

    fun fetchCollections(cityId: Int, count: Int = 10) = viewModelScope.launch {
        _collectionData.postValue(TaskResponse.loading(null))
        if (connectionHelper.isNetworkConnected()) {
            taskRepository.getCollections(cityId, count).let {
                if (it.isSuccessful) {
                    _collectionData.postValue(TaskResponse.success(it.body()))
                } else {
                    _collectionData.postValue(TaskResponse.error(null, "Failed to place network request"))
                }
            }
        } else {
            _collectionData.postValue(TaskResponse.error(data = null, message = "No Internet Connection"))
        }
    }

    // cached
    private val _outletData = MutableLiveData<TaskResponse<OutletData>>()
    // public
    val outletData: LiveData<TaskResponse<OutletData>>
        get() = _outletData

    fun fetchOutletData(cityId: Int) = viewModelScope.launch {
        _outletData.postValue(TaskResponse.loading(null))
        if (connectionHelper.isNetworkConnected()) {
            taskRepository.getLocationDetails(cityId).let {
                if (it.isSuccessful) {
                    _outletData.postValue(TaskResponse.success(it.body()))
                } else {
                    _outletData.postValue(TaskResponse.error(null, "Failed to place network request"))
                }
            }
        } else {
            _outletData.postValue(TaskResponse.error(data = null, message = "No Internet Connection"))
        }
    }
}