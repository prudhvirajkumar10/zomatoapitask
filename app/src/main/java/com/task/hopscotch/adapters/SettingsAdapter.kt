package com.task.hopscotch.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.task.hopscotch.databinding.ItemSettingsBinding
import com.task.hopscotch.models.SettingsItem

class SettingsAdapter: RecyclerView.Adapter<SettingsAdapter.Companion.SettingsViewHolder>() {

    val settings = mutableListOf<SettingsItem>(
        SettingsItem("Add a place", "In case we're missing something"),
        SettingsItem("Places you\'ve added", "See all the places you\'ve added so far"),
        SettingsItem("Find friends", "You need them everywhere"),
        SettingsItem("Edit Profile", "Change your name, description and profile photo"),
        SettingsItem("Notification settings", "Define what alerts and notifications you want to see"),
        SettingsItem("Connected Accounts", "Facebook permission"),
        SettingsItem("Privacy", "Hide your profile from search engines"),
        SettingsItem("Account settings", "Change your email or delete your account")
    )

    companion object {
        class SettingsViewHolder(val binding: ItemSettingsBinding): RecyclerView.ViewHolder(binding.root)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingsViewHolder =
        SettingsViewHolder(ItemSettingsBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: SettingsViewHolder, position: Int) {
        holder.binding.apply {
            titleLabel.text = settings[position].title
            description.text = settings[position].description
        }
    }

    override fun getItemCount(): Int = settings.size
}