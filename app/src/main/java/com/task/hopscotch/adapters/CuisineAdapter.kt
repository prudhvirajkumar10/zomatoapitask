package com.task.hopscotch.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.task.hopscotch.R
import com.task.hopscotch.databinding.ItemTopCuisinesBinding

class CuisineAdapter : RecyclerView.Adapter<CuisineAdapter.Companion.CuisineViewHolder>() {

    var data = mutableListOf<String>()

    companion object {
        class CuisineViewHolder(val binding: ItemTopCuisinesBinding) :
            RecyclerView.ViewHolder(binding.root)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CuisineViewHolder =
        CuisineViewHolder(
            ItemTopCuisinesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: CuisineViewHolder, position: Int) {
        holder.binding.apply {
            cuisine.text = data[position]
            when(data[position]) {
                "Fast Food" -> thumbnail.setImageResource(R.drawable.fastfood)
                "North Indian" -> thumbnail.setImageResource(R.drawable.northindian)
                "Beverages" -> thumbnail.setImageResource(R.drawable.beverages)
                "Chinese" -> thumbnail.setImageResource(R.drawable.chinese)
                "Desserts" -> thumbnail.setImageResource(R.drawable.desserts)
            }
        }
    }

    override fun getItemCount(): Int = data.size
}