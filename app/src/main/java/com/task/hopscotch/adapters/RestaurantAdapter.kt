package com.task.hopscotch.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.task.hopscotch.R
import com.task.hopscotch.databinding.ItemNearbyBinding
import com.task.hopscotch.models.Restaurant

class RestaurantAdapter: RecyclerView.Adapter<RestaurantAdapter.Companion.RestaurantViewHolder>(){

    var restaurants = mutableListOf<Restaurant>()

    companion object {
        class RestaurantViewHolder(val binding: ItemNearbyBinding): RecyclerView.ViewHolder(binding.root)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantViewHolder =
        RestaurantViewHolder(ItemNearbyBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) {
        val item = restaurants[position]
        holder.binding.apply {
            titleLabel.text = item.name
            cuisineLabel.text = item.cuisines
            discountLabel.text = discountLabel.context.getString(R.string.discount_label, item.currency, item.averageCostForTwo, 2)
            rating.text = item.userRating.aggregateRating
            minsLabel.text = minsLabel.context.getString(R.string.minsLabel, position * 10)
            addressLabel.text = item.location.localityVerbose
            Glide.with(collectionBackground.context)
                .load(item.featuredImage)
                .centerCrop()
                .into(collectionBackground)
        }
    }

    override fun getItemCount(): Int = restaurants.size

}