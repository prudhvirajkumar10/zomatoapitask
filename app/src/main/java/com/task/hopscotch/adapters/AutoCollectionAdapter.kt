package com.task.hopscotch.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.task.hopscotch.R
import com.task.hopscotch.databinding.ItemCollectionBinding
import com.task.hopscotch.models.CollectionCollection

class AutoCollectionAdapter: RecyclerView.Adapter<AutoCollectionAdapter.Companion.AutoCollectionViewHolder>() {

    var autoCollectionData = mutableListOf<CollectionCollection>()

    companion object {
        class AutoCollectionViewHolder(val binding: ItemCollectionBinding): RecyclerView.ViewHolder(binding.root)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AutoCollectionViewHolder =
            AutoCollectionViewHolder(ItemCollectionBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: AutoCollectionViewHolder, position: Int) {
        val item = autoCollectionData[position]
        holder.binding.apply {
            title.text = item.title
            description.text = item.description
            availability.text = availability.context.getString(R.string.restaurant_available, item.resCount)
            Glide.with(collectionBackground.context)
                .load(item.imageURL)
                .centerCrop()
                .into(collectionBackground)
        }
    }

    override fun getItemCount(): Int = autoCollectionData.size
}