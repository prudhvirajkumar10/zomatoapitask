package com.task.hopscotch.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.task.hopscotch.databinding.ItemManualOutletBinding
import com.task.hopscotch.models.ManualOutlet
import com.task.hopscotch.models.Restaurant

class ManualOutletAdapter: RecyclerView.Adapter<ManualOutletAdapter.Companion.ManualOutletViewHolder>() {

    var manualOutletData = mutableListOf<ManualOutlet>()

    companion object {
        class ManualOutletViewHolder(val binding: ItemManualOutletBinding): RecyclerView.ViewHolder(binding.root)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ManualOutletViewHolder =
        ManualOutletViewHolder(ItemManualOutletBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ManualOutletViewHolder, position: Int) {
        val restaurant = manualOutletData[position].restaurants
        val item1 = restaurant.first()
        var item2: Restaurant? = null
        if (restaurant.size > 1) {
            item2 = restaurant.last()
        }
        holder.binding.apply {
            title1.text = item1.name
            Glide.with(outletBg1.context)
                .load(item1.thumb)
                .centerCrop()
                .into(outletBg1)

            item2?.let {
                item2Layout.visibility = View.VISIBLE
                title2.text = it.name
                Glide.with(outletBg2.context)
                    .load(it.thumb)
                    .centerCrop()
                    .into(outletBg2)
            } ?: run {
                item2Layout.visibility = View.GONE
            }
        }
    }

    override fun getItemCount(): Int = manualOutletData.size
}