package com.task.hopscotch.di

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.task.hopscotch.BuildConfig
import com.task.hopscotch.adapters.*
import com.task.hopscotch.network.APIService
import com.task.hopscotch.network.TaskNetworkHelper
import com.task.hopscotch.network.TaskNetworkService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class AppModule {

    @Provides
    fun provideGson(): Gson = GsonBuilder().setLenient().create()

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient
                .Builder()
                .followRedirects(false)
                .addNetworkInterceptor(StethoInterceptor())
                .addNetworkInterceptor { chain ->
                    val requestBuilder = chain.request().newBuilder()
                    requestBuilder.addHeader(APIService.USER_KEY, BuildConfig.ZOMATO_API_KEY)
                    return@addNetworkInterceptor chain.proceed(requestBuilder.build())
                }
        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(loggingInterceptor)
        }
        builder.connectTimeout(30, TimeUnit.SECONDS)
        builder.readTimeout(30, TimeUnit.SECONDS)
        builder.writeTimeout(30, TimeUnit.SECONDS)
        return builder.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit =
            Retrofit.Builder()
                    .baseUrl(BuildConfig.API_BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): APIService =
            retrofit.create(APIService::class.java)

    @Provides
    @Singleton
    fun provideNetworkServiceHelper(taskNetworkService: TaskNetworkService): TaskNetworkHelper = taskNetworkService

    @Provides
    @Singleton
    fun provideAutoCollectionAdapter(): AutoCollectionAdapter = AutoCollectionAdapter()

    @Provides
    @Singleton
    fun provideManualOutletAdapter(): ManualOutletAdapter = ManualOutletAdapter()

    @Provides
    @Singleton
    fun provideCuisineAdapter(): CuisineAdapter = CuisineAdapter()

    @Provides
    @Singleton
    fun provideRestaurantAdapter(): RestaurantAdapter = RestaurantAdapter()

    @Provides
    @Singleton
    fun providesSettingsAdapter(): SettingsAdapter = SettingsAdapter()
}