package com.task.hopscotch.network

import com.task.hopscotch.models.CityData
import com.task.hopscotch.models.CollectionData
import com.task.hopscotch.models.OutletData
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TaskNetworkService @Inject constructor(
        private val apiService: APIService) : TaskNetworkHelper {

    override suspend fun getCity(latitude: Double, longitude: Double): Response<CityData> =
            apiService.getCity(latitude, longitude)

    override suspend fun getCollections(cityId: Int, count: Int): Response<CollectionData> =
            apiService.getCollections(cityId, count)

    override suspend fun getLocationDetails(cityId: Int, type: String): Response<OutletData> =
        apiService.getLocationDetails(cityId, type)
}