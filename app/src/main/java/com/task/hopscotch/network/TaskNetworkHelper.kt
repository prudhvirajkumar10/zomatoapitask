package com.task.hopscotch.network

import com.task.hopscotch.models.CityData
import com.task.hopscotch.models.CollectionData
import com.task.hopscotch.models.OutletData
import retrofit2.Response

interface TaskNetworkHelper {

    suspend fun getCity(latitude: Double, longitude: Double) : Response<CityData>

    suspend fun getCollections(cityId: Int, count: Int): Response<CollectionData>

    suspend fun getLocationDetails(cityId: Int, type: String): Response<OutletData>
}