package com.task.hopscotch.network

import com.task.hopscotch.models.CityData
import com.task.hopscotch.models.CollectionData
import com.task.hopscotch.models.OutletData
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface APIService {

    companion object {
        const val USER_KEY = "user-key"
    }

    @GET("geocode")
    suspend fun getCity(@Query("lat") latitude: Double, @Query("lon") longitude: Double): Response<CityData>

    @GET("collections")
    suspend fun getCollections(@Query("city_id") cityId: Int, @Query("count") count: Int): Response<CollectionData>

    @GET("location_details")
    suspend fun getLocationDetails(@Query("entity_id") cityId: Int, @Query("entity_type") type: String): Response<OutletData>
}