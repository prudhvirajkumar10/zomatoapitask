package com.task.hopscotch.models

import java.io.Serializable

data class ManualOutlet(
    val restaurants: List<Restaurant>
): Serializable