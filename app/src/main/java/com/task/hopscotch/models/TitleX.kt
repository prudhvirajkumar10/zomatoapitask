package com.task.hopscotch.models


import com.google.gson.annotations.SerializedName

data class TitleX(
    @SerializedName("text")
    val text: String
)