package com.task.hopscotch.models

enum class ResponseStatus { SUCCESS, ERROR, LOADING }