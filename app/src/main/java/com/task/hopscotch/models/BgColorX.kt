package com.task.hopscotch.models


import com.google.gson.annotations.SerializedName

data class BgColorX(
    @SerializedName("tint")
    val tint: String,
    @SerializedName("type")
    val type: String
)