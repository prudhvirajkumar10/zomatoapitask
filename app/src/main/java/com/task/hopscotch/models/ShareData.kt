package com.task.hopscotch.models


import com.google.gson.annotations.SerializedName

data class ShareData(
    @SerializedName("should_show")
    val shouldShow: Int
)