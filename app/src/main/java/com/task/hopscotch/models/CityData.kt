package com.task.hopscotch.models


import com.google.gson.annotations.SerializedName

data class CityData(
    @SerializedName("link")
    val link: String,
    @SerializedName("location")
    val location: LocationX,
    @SerializedName("nearby_restaurants")
    val nearbyRestaurants: List<NearbyRestaurant>,
    @SerializedName("popularity")
    val popularity: Popularity
)