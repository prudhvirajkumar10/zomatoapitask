package com.task.hopscotch.models


import com.google.gson.annotations.SerializedName

data class Type(
    @SerializedName("color")
    val color: String,
    @SerializedName("name")
    val name: String
)