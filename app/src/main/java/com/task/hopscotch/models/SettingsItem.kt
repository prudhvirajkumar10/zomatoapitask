package com.task.hopscotch.models

import java.io.Serializable

data class SettingsItem(
    val title: String,
    val description: String
): Serializable