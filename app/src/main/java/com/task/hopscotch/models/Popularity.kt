package com.task.hopscotch.models


import com.google.gson.annotations.SerializedName

data class Popularity(
    @SerializedName("city")
    val city: String,
    @SerializedName("nearby_res")
    val nearbyRes: List<String>,
    @SerializedName("nightlife_index")
    val nightlifeIndex: String,
    @SerializedName("nightlife_res")
    val nightlifeRes: String,
    @SerializedName("popularity")
    val popularity: String,
    @SerializedName("popularity_res")
    val popularityRes: String,
    @SerializedName("subzone")
    val subzone: String,
    @SerializedName("subzone_id")
    val subzoneId: Int,
    @SerializedName("top_cuisines")
    val topCuisines: List<String>
)