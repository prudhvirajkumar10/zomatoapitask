package com.task.hopscotch.models

data class TaskResponse<out T>(val responseStatus: ResponseStatus, val data: T?, val message: String?){
    companion object {
        fun <T> success(data: T?): TaskResponse<T> =
            TaskResponse(
                responseStatus = ResponseStatus.SUCCESS,
                data = data,
                message = null)

        fun <T> error(data: T?, message: String): TaskResponse<T> =
            TaskResponse(
                responseStatus = ResponseStatus.ERROR,
                data = data,
                message = message)

        fun <T> loading(data: T?): TaskResponse<T> =
            TaskResponse(
                responseStatus = ResponseStatus.LOADING,
                data = data,
                message = null)
    }
}