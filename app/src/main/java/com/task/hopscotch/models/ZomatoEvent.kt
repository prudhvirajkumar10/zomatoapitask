package com.task.hopscotch.models


import com.google.gson.annotations.SerializedName

data class ZomatoEvent(
    @SerializedName("event")
    val event: Event
)