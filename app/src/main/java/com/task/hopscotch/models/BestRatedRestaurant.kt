package com.task.hopscotch.models


import com.google.gson.annotations.SerializedName

data class BestRatedRestaurant(
    @SerializedName("restaurant")
    val restaurant: Restaurant
)