package com.task.hopscotch.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class CollectionData (
        val collections: List<CollectionElement>,

        @SerializedName("has_more")
        val hasMore: Long,

        @SerializedName("share_url")
        val shareURL: String,

        @SerializedName("display_text")
        val displayText: String,

        @SerializedName("has_total")
        val hasTotal: Long,

        @SerializedName("user_has_addresses")
        val userHasAddresses: Boolean
): Serializable

data class CollectionElement (
        val collection: CollectionCollection
): Serializable

data class CollectionCollection (
        @SerializedName("collection_id")
        val collectionID: Long,

        @SerializedName("res_count")
        val resCount: Long,

        @SerializedName("image_url")
        val imageURL: String,

        val url: String,
        val title: String,
        val description: String,

        @SerializedName("share_url")
        val shareURL: String
): Serializable
